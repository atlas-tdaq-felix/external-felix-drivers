// -*- c++ -*-
/************************************************/
/* file: GlobalDebugSettings.h			*/
/* description: Class GlobalDebugSettings	*/
/* maintainer: Markus Joos, CERN/PH-ESS		*/
/************************************************/

#ifndef DFGLOBALDEBUGSETTINGS_H
#define DFGLOBALDEBUGSETTINGS_H

#include <mutex>
#include <iostream>
#include <string>

namespace DF 
{
  class GlobalDebugSettings 
  {
    public:
      static std::mutex s_debug_mutex;
      static unsigned int traceLevel();
      static unsigned int packageId();
      static void setup(unsigned int traceLevel,unsigned int packageId = 0); 
    private:
      static unsigned int s_traceLevel;
      static unsigned int s_packageId;
  };

  inline unsigned int GlobalDebugSettings::traceLevel() 
  {
    return(s_traceLevel);
  }

  inline unsigned int GlobalDebugSettings::packageId() 
  {
    return(s_packageId);
  }

  inline void GlobalDebugSettings::setup(unsigned int traceLevel, unsigned int packageId) 
  {
    s_traceLevel = traceLevel;
    s_packageId = packageId;
  }

} // end of namespace DF
#endif //DFGLOBALDEBUGSETTINGS_H

