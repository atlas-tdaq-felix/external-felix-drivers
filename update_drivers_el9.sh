#!/bin/sh

VERSION=4.19.0
DASH_VERSION=04-19-00
OS=el9
BINARY_TAG=x86_64-el9-gcc13-opt

NAME=felix-drivers

mkdir -p ${VERSION}
(cd ${VERSION}; tar zxvf /eos/project/f/felix/www/user/dist/software/drivers/releases/${OS}/${NAME}-${DASH_VERSION}/${NAME}-${DASH_VERSION}-lib-${BINARY_TAG}.tar.gz)

