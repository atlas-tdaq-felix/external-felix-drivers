#!/bin/sh

VERSION=4.14.0
DASH_VERSION=04-14-00
OS=centos7
BINARY_TAG=x86_64-centos7-gcc11-opt

NAME=felix-drivers

mkdir -p ${VERSION}
(cd ${VERSION}; tar zxvf /eos/project/f/felix/www/user/dist/software/drivers/releases/${OS}/${NAME}-${DASH_VERSION}/${NAME}-${DASH_VERSION}-lib-${BINARY_TAG}.tar.gz)

